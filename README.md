# MyWorks

C#/.NET course.

Course program:

1) Console Programs

2) SQL

3) Decomposition

4) ASP.NET

5) WPF (WinForms)

6) .NET 6


Skills for the course: 

1) Knowledge of basic C# syntax;
2) Information search skills;
3) Knowledge of Object-oriented programming;
4) Knowledge of English languages ??at the level of reading technical documentation.